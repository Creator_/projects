import React, {FC, useState} from 'react';
import {Route, Routes, useLocation} from 'react-router-dom';
import './App.css';
import Navbar from "./components/navbar/Navbar";
import Projects from "./components/projects/Projects";
import Staff from "./components/staff/Staff";
import Tasks from "./components/tasks/Tasks";
import MyModal from "./UI/modal/MyModal";
import ProjectForm from "./UI/form/ProjectForm";
import TasksForm from "./UI/form/TasksForm";
import StaffForm from "./UI/form/StaffForm";

const App: FC = () => {

  const [tasks, setTasks] = useState([{
    id: 0,
    title: 'test',
    work: 'start',
    start: Date.now(),
    end: Date.now(),
    status: 'inProcess'
  }])

  const [staff, setStaff] = useState([{
    id: 0,
    surName: 'Marcus',
    firstName: 'Sayo',
    lastName: 'John',
    position: 'Mayor'

  }])


  const [projects, setProjects] = useState([{
    id: 0,
    title: 'firstProject',
    description: 'some texts'
  }])

  const [visible, setVisible] = useState(false)
  const location = useLocation();

  const onDeleteStaff = (id: number) => {
    setStaff(staff.filter(item => item.id !== id))
  }

  const onDeleteProjects = (id: number) => {
    setProjects(projects.filter(project => project.id !== id))
  }
  const onDeleteTasks = (id: number) => {
    setTasks(tasks.filter(task => task.id !== id))
  }

  const currentForm = () => {
    switch (location.pathname) {
    case '/projects':
      return <ProjectForm/>
    case '/tasks':
      return <TasksForm/>
    case '/staff':
      return <StaffForm/>
    }
  }

  return (
    <div>
      <button onClick={() => setVisible(true)}>+</button>
      <MyModal visible={visible} setVisible={setVisible}> {currentForm()} </MyModal>
      <Routes>
        <Route path='/projects' element={projects.length > 0 ? projects.map(project => <Projects
          key={project.id}
          description={project.description}
          title={project.title}
          setProjects={setProjects}
          project={project}
          onDeleteProjects={onDeleteProjects}
        />) : 'проекты отсутствуют'}/>

        <Route path='/staff' element={staff.length > 0 ? staff.map(item => <Staff
          key={item.id}
          firstName={item.firstName}
          id={item.id}
          lastName={item.lastName}
          position={item.position}
          surName={item.surName}
          onDeleteStaff={onDeleteStaff}
        />) : 'Добавте сотрудника'}/>

        <Route path='/Tasks' element={tasks.length > 0 ? tasks.map(task => <Tasks
          key={task.id}
          title={task.title}
          id={task.id}
          status={task.status}
          work={task.work}
          onDeleteTasks={onDeleteTasks}
        />) : 'Задач нет'}/>
      </Routes>
      <Navbar/>
    </div>
  );
}

export default App;
