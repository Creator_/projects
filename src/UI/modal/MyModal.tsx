import React, {FC} from 'react';
import mod from './myModal.module.css'

interface propsModal {
    visible: boolean,
    setVisible: Function,
    children: React.ReactNode
}

const MyModal:FC<propsModal> = ({visible, setVisible, children}) => {

  const rootClasses = [mod.myModal]

  if (visible) {
    rootClasses.push(mod.active)
  }

  return (
    <div className={rootClasses.join(' ')} onClick={() => setVisible(false) } >
      <div className={mod.content} onClick={event => event.stopPropagation() }>
        {children}
      </div>
    </div>
  );
};

export default MyModal;