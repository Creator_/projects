import React, {FC} from 'react';

interface staffProps {
    id: number,
    surName: string,
    firstName: string,
    lastName: string,
    position: string,
    onDeleteStaff: Function

}

const Staff: FC<staffProps> = ({surName, firstName, lastName, position, id, onDeleteStaff}) => {
  return (
    <table cellSpacing='1' cellPadding='1' border={3}>
      <tr>
        <th>Фамилия</th>
        <th>Имя</th>
        <th>Отчество</th>
        <th>Должность</th>
        <th>Действие</th>
      </tr>
      <tr>
        <td> {surName}</td>
        <td> {firstName}</td>
        <td> {lastName}</td>
        <td> {position}</td>
        <td>
          <button>edit</button>
          <button onClick={() => onDeleteStaff(id)}>delete</button>
        </td>
      </tr>
      <tr>
        <td> 7</td>
        <td> 8</td>
        <td> 9</td>
        <td> 9</td>
      </tr>

    </table>
  );
};

export default Staff;