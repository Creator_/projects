import React, {FC} from 'react';
import {NavLink} from "react-router-dom";

const Navbar:FC = () => {
  return (
    <div>
      <NavLink to='/projects'> проекты</NavLink>
      <NavLink to='/staff'> сотрудники</NavLink>
      <NavLink to='/tasks'> задачи</NavLink>
    </div>
  );
};

export default Navbar;