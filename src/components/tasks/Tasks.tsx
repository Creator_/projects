import React, {FC, useState} from 'react';


interface tasksProps {
    id: number,
    title: string,
    work: string,
    status: string,
    onDeleteTasks: Function
}

const Tasks: FC<tasksProps> = ({title, status, work, id, onDeleteTasks}) => {

  return (
    <table cellSpacing='6' border={3} cellPadding='5'>
      <tr>
        <th>Наименование</th>
        <th>Работа</th>
        <th>Дата начала</th>
        <th>Дата окончания</th>
        <th>Статус</th>
        <th>Действие</th>
      </tr>

      <tr>
        <td>{title}</td>
        <td> {work}</td>
        <td> {}</td>
        <td></td>
        <td> status</td>
        <td>
          <button>edit</button>
          <button onClick={() => onDeleteTasks(id)}>delete</button>
        </td>
      </tr>

    </table>
  );
};

export default Tasks;