import React, {FC} from 'react';

interface projectsProps {
    title: string,
    description: string,
    setProjects: any,
    project: any,
    onDeleteProjects: Function

}

const Projects: FC<projectsProps> = ({title, description, setProjects, project, onDeleteProjects}) => {


  return (
    <>
      <table border={3} cellSpacing='4' cellPadding='3'>
        <tr>
          <td>Наименование</td>
          <td>Описание</td>
          <td> Список задач</td>
          <td>Действие</td>

        </tr>
        <tr>
          <td>{title}</td>
          <td>{description}</td>
          <td>
            <table cellSpacing='6' border={1} cellPadding='5'>
              <tr>
                <th>Наименование</th>
                <th>Работа</th>
                <th>Дата начала</th>
                <th>Дата окончания</th>
                <th>Статус</th>
              </tr>
              <tr>
                <td>1</td>
                <td>2</td>
                <td><input type='date'/></td>
                <td><input type='date'/></td>
                <td>5</td>
              </tr>
            </table>
          </td>
          <td>
            <button>edit</button>
            <button onClick={() => onDeleteProjects(project.id)}>delete</button>
          </td>


        </tr>

      </table>
    </>
  );
};

export default Projects;